package rainbow.authz

default allow = false

allow = true {
    input.method = "GET"
    input.path = "/v1/health"
}

allow = true {
    input.method = "POST"
    input.path = "/v1/email/newcode"
}

allow = true {
    input.method = "PUT"
    input.path = "/v1/email/validate"
}

allow = true {
    input.method = "POST"
    input.path = "/v1/pass/newcode"
}

allow = true {
    input.method = "PUT"
    input.path = "/v1/pass/reset"
}

allow = true {
    input.method = "PUT"
    input.path = "/v1/account/pass/update"
    input.role = "user"
}

allow = true {
    input.method = "POST"
    input.path = "/v1/account/signin"
}

allow = true {
    input.method = "PUT"
    input.path = "/v1/account/login"
}

allow = true {
    input.method = "PUT"
    input.path = "/v1/account/suspend"
}

allow = true {
    input.method = "PUT"
    input.path = "/v1/account/unsuspend"
}

allow = true {
    input.method = "PUT"
    input.path = "/v1/account/restore"
}

allow = true {
    input.method = "PUT"
    input.path = "/v1/account/delete"
}

allow = true {
    input.method = "GET"
    input.path = "/v1/profile"
    input.role = "user"
}

allow = true {
    input.method = "DELETE"
    input.path = "/v1/profile"
    input.role = "user"
}

allow = true {
    input.method = "PATCH"
    input.path = "/v1/profile/info"
    input.role = "user"
}

allow = true {
    input.method = "PATCH"
    input.path = "/v1/profile/avatar"
    input.role = "user"
}

