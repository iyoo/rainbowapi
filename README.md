# Rainbow API

## Summary

I'm developing this project to learn GO and I'm new to GO.

I'm open to comments on how to make this code better. If you feel
like helping and see something you believe I should change please open a new ISSUE, so we can discuss it :D

Rainbow is a simple API Service. Running it you will get a few endpoints to
create, update and delete an user account/profile.

## Features

* Create a new account
* Mark to delete/Restore Account
* Suspend/Unsuspend Account
* Update account password
* Send an email using templates to confirm account email
* Password Reset
* Send an email using templates to confirm account reset password
* Generate a JWTToken
* Health check endpoint
* Set/Update/Delete a profile Info. Including a profile avatar
* Policy-based control for cloud native environments made by [Opa](https://www.openpolicyagent.org/) <3
* API documented using openapi
* Postman Collection
* A docker-compose file created to run all the services you need to run rainbow
* Configure it through environment variables

## API

[OpenAPI specification](./api/openapi.json)

## OPA

[Learn more about The Open Policy Agent](https://www.openpolicyagent.org/)

## Infra to run this service

| Service/Instance | Container name | Ports | What for |
| --- | --- | --- | --- |
| Redis | redis-email | 6379 | Temporarily save an uuid code to confirm an account email |
| Redis | redis-pass | 6380 | Temporarily save an uuid code to confirm a password reset |
| Redis Commander | redis-commander | 8081 | A Redis web client to have access to both Redis instances |
| MailHog | smtp-server | 1025/8025 | An "smtp-server"/"web-client" to check emails sent by Rainbow |
| Mysql | mysql-server | 3306 | Save account and profile data |
| PhpMyAdmin | phpmyadmin | 8080 | Mysql Web client |
| Opa | opa | 8181 | Policy-based control |

> You can get more information on usernames and passwords on the docker-compose.yml file

## Extra files

* There is a .env file to configure the entire environment
* Policy-Base control is made by deployments/opa/authz.rego
* The docs folder contains the openapi specification to Rainbow
* The docs folder contains a Postman collection to play with Rainbow's API

## More

Clone Rainbow API git repository:

```
go get gitlab.com/rainbowproject/rainbow-api
```

Spin up the docker containers

```
make docker-compose-up
```

Build Rainbow API

```
make
```

Run Rainbow API

```
./bin/rainbow
```

Update openapi specification
```
make openapi-update
```

Run swagger web-server
```
make openapi-run-server
```

## Dependences

| Dependence | Reason |
| ---: | :--- |
| [jwt-go](https://github.com/dgrijalva/jwt-go) | Generate and Verify JWTTokens |
| [crypto](https://golang.org/x/crypto) | Salting passwords |
| [validator](https://github.com/go-playground/validator) | Validate JSON inputs |
| [go-redis](https://github.com/go-redis/redis) | Interface to Redis Servers |
| [mysql](https://github.com/go-sql-driver/mysql) | Interface to MySQL Server |
| [uuid](https://github.com/google/uuid) | Generate UUID numbers |
| [gorilla mux](https://github.com/gorilla/mux) | Gorilla Mux for routes |
| [zerolog](https://github.com/rs/zerolog) | Logging system |
| [viper](https://github.com/spf13/viper) | Config system |
