module gitlab.com/rainbowproject/rainbow-api

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/go-redis/redis v6.15.8+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.4
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/onsi/ginkgo v1.14.0 // indirect
	github.com/rs/zerolog v1.19.0
	github.com/spf13/viper v1.7.0
	golang.org/x/crypto v0.0.0-20200709230013-948cd5f35899
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
)
