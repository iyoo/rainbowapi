package main

import (
	"github.com/rs/zerolog/log"

	"gitlab.com/rainbowproject/rainbow-api/internal/logging"
	"gitlab.com/rainbowproject/rainbow-api/internal/server"
)

var (
	commit  string
	builtAt string
	builtBy string
	builtOn string
)

func main() {
	logging.InitLogger()
	printVersion()
	server.Start()
}

func printVersion() {
	log.Info().Msgf("Version info :: commit: %s built @ %s by %s on %s",
		commit, builtAt, builtBy, builtOn)
}
