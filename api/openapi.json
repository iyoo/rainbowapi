{
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "schemes": [
    "http",
    "https"
  ],
  "swagger": "2.0",
  "info": {
    "description": "the purpose of this application is to provide endpoints\nto create users and return a token to a valid and authenticated user",
    "title": "Rainbow USER API",
    "contact": {
      "name": "Thiago Mendes",
      "url": "https://rainbowproject.gitlab.io/28lbackpack/",
      "email": "rainbowproject@protonmail.com"
    },
    "license": {
      "name": "MIT",
      "url": "http://opensource.org/licenses/MIT"
    },
    "version": "0.0.1"
  },
  "host": "localhost:3000",
  "basePath": "/",
  "paths": {
    "/v1/account/delete": {
      "put": {
        "description": "Mark account to be deleted",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "tags": [
          "account-api"
        ],
        "operationId": "Delete",
        "parameters": [
          {
            "description": "Data to delete an account",
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/AccountChangeStatusReq"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success Message",
            "schema": {
              "$ref": "#/definitions/Suc"
            }
          }
        }
      }
    },
    "/v1/account/login": {
      "put": {
        "description": "Login into the system",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "tags": [
          "account-api"
        ],
        "operationId": "Login",
        "parameters": [
          {
            "description": "Login Request",
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/LoginReq"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success Message",
            "schema": {
              "$ref": "#/definitions/Suc"
            }
          }
        }
      }
    },
    "/v1/account/pass": {
      "put": {
        "security": [
          {
            "Bearer": []
          }
        ],
        "description": "Update account password",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "tags": [
          "account-api"
        ],
        "operationId": "UpdatePass",
        "parameters": [
          {
            "description": "Data to update account password",
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/UpdatePassReq"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success Message",
            "schema": {
              "$ref": "#/definitions/Suc"
            }
          }
        }
      }
    },
    "/v1/account/restore": {
      "put": {
        "description": "Try to restore account if is not already deleted",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "tags": [
          "account-api"
        ],
        "operationId": "Restore",
        "parameters": [
          {
            "description": "Data to try to restore an account",
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/AccountChangeStatusReq"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success Message",
            "schema": {
              "$ref": "#/definitions/Suc"
            }
          }
        }
      }
    },
    "/v1/account/signin": {
      "post": {
        "description": "Create a new account",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "tags": [
          "account-api"
        ],
        "operationId": "SignIn",
        "parameters": [
          {
            "description": "Data to register a new account into the system",
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/AccountRegistrationReq"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success Message",
            "schema": {
              "$ref": "#/definitions/Suc"
            }
          }
        }
      }
    },
    "/v1/account/suspend": {
      "put": {
        "description": "Suspend account",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "tags": [
          "account-api"
        ],
        "operationId": "Suspend",
        "parameters": [
          {
            "description": "Data to suspend an account",
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/AccountChangeStatusReq"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success Message",
            "schema": {
              "$ref": "#/definitions/Suc"
            }
          }
        }
      }
    },
    "/v1/account/unsuspend": {
      "put": {
        "description": "Unsuspend account",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "tags": [
          "account-api"
        ],
        "operationId": "Unsuspend",
        "parameters": [
          {
            "description": "Data to unsuspend an account",
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/AccountChangeStatusReq"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success Message",
            "schema": {
              "$ref": "#/definitions/Suc"
            }
          }
        }
      }
    },
    "/v1/email/newcode": {
      "post": {
        "description": "Generate a new confirmation code to send by email",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "tags": [
          "account-email-api"
        ],
        "operationId": "NewEmailCode",
        "parameters": [
          {
            "description": "accout email to generate new code",
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/CodeReq"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success Message",
            "schema": {
              "$ref": "#/definitions/Suc"
            }
          }
        }
      }
    },
    "/v1/email/validate": {
      "put": {
        "description": "Validate an account email address",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "tags": [
          "account-email-api"
        ],
        "operationId": "ValidateEmail",
        "parameters": [
          {
            "description": "The account UUID got by email",
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/EmailValidationReq"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success Message",
            "schema": {
              "$ref": "#/definitions/Suc"
            }
          }
        }
      }
    },
    "/v1/health": {
      "get": {
        "description": "Return an ok message if the service is up and running",
        "produces": [
          "application/json"
        ],
        "tags": [
          "health-api"
        ],
        "operationId": "Health",
        "responses": {
          "200": {
            "description": "Success Message",
            "schema": {
              "$ref": "#/definitions/Suc"
            }
          }
        }
      }
    },
    "/v1/pass/newcode": {
      "post": {
        "description": "Request a password reset",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "tags": [
          "account-pass-reset-api"
        ],
        "operationId": "NewResetPassCode",
        "parameters": [
          {
            "description": "UUID got by email",
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/CodeReq"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success Message",
            "schema": {
              "$ref": "#/definitions/Suc"
            }
          }
        }
      }
    },
    "/v1/pass/reset": {
      "put": {
        "description": "Reset account password",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "tags": [
          "account-pass-reset-api"
        ],
        "operationId": "ResetPass",
        "parameters": [
          {
            "description": "Data to update account password",
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/ResetPassReq"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success Message",
            "schema": {
              "$ref": "#/definitions/Suc"
            }
          }
        }
      }
    },
    "/v1/profile": {
      "get": {
        "security": [
          {
            "Bearer": []
          }
        ],
        "description": "Fetch account profile",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "tags": [
          "profile-api"
        ],
        "operationId": "FetchProfile",
        "responses": {
          "200": {
            "description": "Success Message",
            "schema": {
              "$ref": "#/definitions/Suc"
            }
          }
        }
      },
      "delete": {
        "security": [
          {
            "Bearer": []
          }
        ],
        "description": "Delete account profile",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "tags": [
          "profile-api"
        ],
        "operationId": "UpdateProfile",
        "responses": {
          "200": {
            "description": "Success Message",
            "schema": {
              "$ref": "#/definitions/Suc"
            }
          }
        }
      }
    },
    "/v1/profile/avatar": {
      "patch": {
        "security": [
          {
            "Bearer": []
          }
        ],
        "description": "Update account profile avatar",
        "consumes": [
          "multipart/form-data"
        ],
        "produces": [
          "application/json"
        ],
        "tags": [
          "profile-api"
        ],
        "operationId": "UpdateAvatar",
        "parameters": [
          {
            "type": "file",
            "description": "The uploaded avatar image data",
            "name": "avatar",
            "in": "formData",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "Success Message",
            "schema": {
              "$ref": "#/definitions/Suc"
            }
          }
        }
      }
    },
    "/v1/profile/info": {
      "patch": {
        "security": [
          {
            "Bearer": []
          }
        ],
        "description": "Update account profile info",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "tags": [
          "profile-api"
        ],
        "operationId": "UpdateProfile",
        "parameters": [
          {
            "description": "profile info",
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/ProfileInfoReq"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success Message",
            "schema": {
              "$ref": "#/definitions/Suc"
            }
          }
        }
      }
    }
  },
  "definitions": {
    "AccountChangeStatusReq": {
      "description": "Request to Delete/Restore/Suspend/Unsuspend an account",
      "type": "object",
      "required": [
        "email",
        "password"
      ],
      "properties": {
        "email": {
          "description": "Email address",
          "type": "string",
          "uniqueItems": true,
          "x-go-name": "Email",
          "example": "dummy@dummy.com"
        },
        "password": {
          "description": "Password",
          "type": "string",
          "minLength": 10,
          "x-go-name": "Password",
          "example": "dummy12345678"
        }
      },
      "x-go-package": "gitlab.com/rainbowproject/rainbow-api/internal/models/rest"
    },
    "AccountRegistrationReq": {
      "description": "New User Request",
      "type": "object",
      "required": [
        "email",
        "password",
        "confirm_password"
      ],
      "properties": {
        "confirm_password": {
          "description": "Password confirmation",
          "type": "string",
          "minLength": 10,
          "x-go-name": "ConfirmPassword",
          "example": "dummy12345678"
        },
        "email": {
          "description": "Email address",
          "type": "string",
          "uniqueItems": true,
          "x-go-name": "Email",
          "example": "dummy@dummy.com"
        },
        "password": {
          "description": "Password",
          "type": "string",
          "minLength": 10,
          "x-go-name": "Password",
          "example": "dummy12345678"
        }
      },
      "x-go-package": "gitlab.com/rainbowproject/rainbow-api/internal/models/rest"
    },
    "CodeReq": {
      "description": "Email Verification Code Request",
      "type": "object",
      "required": [
        "email"
      ],
      "properties": {
        "email": {
          "description": "Email address",
          "type": "string",
          "uniqueItems": true,
          "x-go-name": "Email",
          "example": "dummy@dummy.com"
        }
      },
      "x-go-package": "gitlab.com/rainbowproject/rainbow-api/internal/models/rest"
    },
    "EmailValidationReq": {
      "description": "Email Verification Request",
      "type": "object",
      "required": [
        "id"
      ],
      "properties": {
        "id": {
          "description": "Code got by email",
          "type": "string",
          "x-go-name": "ID",
          "example": "123e4567-e89b-12d3-a456-426614174000"
        }
      },
      "x-go-package": "gitlab.com/rainbowproject/rainbow-api/internal/models/rest"
    },
    "Err": {
      "description": "Response Error Message",
      "type": "object",
      "required": [
        "message",
        "status",
        "error"
      ],
      "properties": {
        "error": {
          "description": "Error Message summary",
          "type": "string",
          "x-go-name": "Error",
          "example": "\"not_found\""
        },
        "message": {
          "description": "Error Message",
          "type": "string",
          "x-go-name": "Message",
          "example": "Invalid password"
        },
        "status": {
          "description": "Error Message Code",
          "type": "integer",
          "format": "int64",
          "x-go-name": "Status",
          "example": 404
        }
      },
      "x-go-name": "jsonErrorMsg",
      "x-go-package": "gitlab.com/rainbowproject/rainbow-api/internal/models/rest"
    },
    "LoginReq": {
      "description": "Login Request",
      "type": "object",
      "required": [
        "email",
        "password"
      ],
      "properties": {
        "email": {
          "description": "Email Address",
          "type": "string",
          "uniqueItems": true,
          "x-go-name": "Email",
          "example": "dummy@dummy.com"
        },
        "password": {
          "description": "Password",
          "type": "string",
          "minLength": 10,
          "x-go-name": "Password",
          "example": "dummy12345678"
        }
      },
      "x-go-package": "gitlab.com/rainbowproject/rainbow-api/internal/models/rest"
    },
    "ProfileAvatarReq": {
      "description": "Profile Avatar Update Request",
      "type": "object",
      "required": [
        "Avatar"
      ],
      "properties": {
        "Avatar": {
          "description": "Avatar",
          "type": "string",
          "minLength": 5,
          "example": "profile.jpg"
        }
      },
      "x-go-package": "gitlab.com/rainbowproject/rainbow-api/internal/models/rest"
    },
    "ProfileInfoReq": {
      "description": "Profile Info Create/Update Request",
      "type": "object",
      "required": [
        "first_name",
        "last_name",
        "mobile",
        "gender"
      ],
      "properties": {
        "first_name": {
          "description": "FistName\nMax Lenght: 255",
          "type": "string",
          "minLength": 10,
          "x-go-name": "FirstName",
          "example": "\"Dummy\""
        },
        "gender": {
          "description": "Gender\nMax Lenght: 255",
          "type": "string",
          "minLength": 10,
          "x-go-name": "Gender",
          "example": "\"non-binary\""
        },
        "last_name": {
          "description": "LastName\nMax Lenght: 255",
          "type": "string",
          "minLength": 10,
          "x-go-name": "LastName",
          "example": "\"DummyFamily\""
        },
        "mobile": {
          "description": "Mobile\nMax Lenght: 255",
          "type": "string",
          "minLength": 10,
          "x-go-name": "Mobile",
          "example": "\"48123456789\""
        }
      },
      "x-go-package": "gitlab.com/rainbowproject/rainbow-api/internal/models/rest"
    },
    "ProfileInfoResp": {
      "description": "Profile Info Response",
      "type": "object",
      "required": [
        "Avatar",
        "first_name",
        "last_name",
        "mobile",
        "gender"
      ],
      "properties": {
        "Avatar": {
          "description": "Avatar",
          "type": "string",
          "minLength": 5,
          "example": "profile.jpg"
        },
        "first_name": {
          "description": "FistName",
          "type": "string",
          "minLength": 10,
          "x-go-name": "FirstName",
          "example": "\"Dummy\""
        },
        "gender": {
          "description": "Gender",
          "type": "string",
          "minLength": 10,
          "x-go-name": "Gender",
          "example": "\"non-binary\""
        },
        "last_name": {
          "description": "LastName",
          "type": "string",
          "minLength": 10,
          "x-go-name": "LastName",
          "example": "\"DummyFamily\""
        },
        "mobile": {
          "description": "Mobile",
          "type": "string",
          "minLength": 10,
          "x-go-name": "Mobile",
          "example": "\"48123456789\""
        }
      },
      "x-go-package": "gitlab.com/rainbowproject/rainbow-api/internal/models/rest"
    },
    "ResetPassReq": {
      "description": "Reset Password Request. Forgotten password",
      "type": "object",
      "required": [
        "password",
        "new_password",
        "confirm_password",
        "id",
        "email"
      ],
      "properties": {
        "confirm_password": {
          "description": "New Password confirmation",
          "type": "string",
          "x-go-name": "ConfirmPassword",
          "example": "newdummy12345678"
        },
        "email": {
          "description": "Email address",
          "type": "string",
          "uniqueItems": true,
          "x-go-name": "Email",
          "example": "dummy@dummy.com"
        },
        "id": {
          "description": "Code got by email",
          "type": "string",
          "x-go-name": "ID",
          "example": "123e4567-e89b-12d3-a456-426614174000"
        },
        "new_password": {
          "description": "New Password",
          "type": "string",
          "minLength": 10,
          "x-go-name": "NewPassword",
          "example": "newdummy12345678"
        },
        "password": {
          "description": "Password",
          "type": "string",
          "minLength": 10,
          "x-go-name": "Password",
          "example": "dummy12345678"
        }
      },
      "x-go-package": "gitlab.com/rainbowproject/rainbow-api/internal/models/rest"
    },
    "Suc": {
      "description": "Response Success Message",
      "type": "object",
      "required": [
        "message",
        "status",
        "payload"
      ],
      "properties": {
        "message": {
          "description": "Success Message",
          "type": "string",
          "x-go-name": "Message",
          "example": "User created"
        },
        "payload": {
          "description": "Success Payload",
          "type": "object",
          "x-go-name": "Payload",
          "example": {}
        },
        "status": {
          "description": "Success Code",
          "type": "integer",
          "format": "int64",
          "x-go-name": "Status",
          "example": 200
        }
      },
      "x-go-name": "respSuc",
      "x-go-package": "gitlab.com/rainbowproject/rainbow-api/internal/models/rest"
    },
    "TokenResp": {
      "description": "Bearer Token Payload",
      "type": "object",
      "required": [
        "bearer_token"
      ],
      "properties": {
        "bearer_token": {
          "description": "Bearer Token",
          "type": "string",
          "uniqueItems": true,
          "x-go-name": "BearerToken",
          "example": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiRnJlZWRvbSBvZiBleHByZXNzaW9uIiwibWVzc2FnZSI6ImxpdmUgbGlmZSBhcm91bmQgZXhwZXJpZW5jZSByYXRoZXIgdGhhbiBzdHVmZnwiLCJpYXQiOjE1MTYyMzkwMjJ9.2wZG5ykBQuqAg99OnHXJmyzWCBDkrKT2heODjuHxoGw"
        }
      },
      "x-go-package": "gitlab.com/rainbowproject/rainbow-api/internal/models/rest"
    },
    "UpdatePassReq": {
      "description": "Update Password",
      "type": "object",
      "required": [
        "password",
        "new_password",
        "confirm_password"
      ],
      "properties": {
        "confirm_password": {
          "description": "New Password confirmation",
          "type": "string",
          "x-go-name": "ConfirmPassword",
          "example": "newdummy12345678"
        },
        "new_password": {
          "description": "New Password",
          "type": "string",
          "minLength": 10,
          "x-go-name": "NewPassword",
          "example": "newdummy12345678"
        },
        "password": {
          "description": "Password",
          "type": "string",
          "minLength": 10,
          "x-go-name": "Password",
          "example": "dummy12345678"
        }
      },
      "x-go-package": "gitlab.com/rainbowproject/rainbow-api/internal/models/rest"
    }
  },
  "securityDefinitions": {
    "Bearer": {
      "description": "For accessing the API a valid JWT token must be passed in all the queries in\nthe 'Authorization' header.\nA valid JWT token is generated by the API and retourned as answer of a call\nto the route /login giving a valid user \u0026 password.\nThe following syntax must be used in the 'Authorization' header :\n    Bearer xxxxxx.yyyyyyy.zzzzzz",
      "type": "apiKey",
      "name": "Authorization",
      "in": "header"
    }
  }
}