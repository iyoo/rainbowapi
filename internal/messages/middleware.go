package messages

const (
	Forbidden    = "Access Forbidden"
	MissingToken = "An authorization header is required"
	InvalidToken = "Invalid authorization token"
	BrokenToken  = "There is something wrong with your token"
)
