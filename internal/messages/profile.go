package messages

const (
	ProfileDeleted         = "Profile deleted"
	ProfileSucceed         = "Fetch succeed"
	ProfileInfoUpdated     = "Profile info updated"
	ProfileAvatarUpdated   = "Profile avatar updated"
	ProfileAvatarRetError  = "Can't retrieve avatar from request"
	ProfileAvatarMaxSize   = "Avatar maximum file size exceeded"
	ProfileAvatarTempFail  = "Can't create temporary avatar"
	ProfileAvatarReadFail  = "Can't parse uploaded avatar"
	ProfileAvatarWriteFail = "Can't save uploaded avatar"
)
