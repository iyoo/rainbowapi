package messages

const (
	AccountCreated               = "Account created"
	AccountToDelete              = "Account is marked for deletion"
	AccountRestored              = "Account deletion cancelled"
	AuthSaltFail                 = "Fail on Salt Password"
	AccountPassNotMatch          = "Password does not match"
	AccountAlreadyExists         = "Account is already register on the system"
	AuthGenTokenFail             = "Fail on generate JWToken"
	AuthSuccess                  = "Welcome"
	AccountEmailNotValidated     = "Email is not yet validated"
	AccountInvalidPassword       = "Invalid password"
	AccountEmailValidated        = "Account is now validated"
	RedisInvalidKey              = "Invalid validation key"
	AccountEmailAlreadyValidated = "Account Email already verified"
	AccountTokenGenerated        = "New Code generated"
	AccountPassUpdated           = "Password updated"
	AccountSuspended             = "Account is suspended"
	AccountUnsuspended           = "Account is unsuspended"
)
