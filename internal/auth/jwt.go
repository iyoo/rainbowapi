package auth

import (
	"time"

	"gitlab.com/rainbowproject/rainbow-api/internal/config"
	"gitlab.com/rainbowproject/rainbow-api/internal/models/internals"

	"github.com/dgrijalva/jwt-go"
)

type TokenHandler interface {
	Create(internals.AuthTokenPayload) (string, error)
	Verify(string) (*internals.AuthTokenPayload, error)
}

type claims struct {
	Auth internals.AuthTokenPayload
	jwt.StandardClaims
}

type jwtManagement struct {
	jKey       []byte
	expireTime int
}

func NewTokenHandler() TokenHandler {
	jwtConfig := config.GetJWTEnvVar()
	secret := jwtConfig.Secret
	return &jwtManagement{
		expireTime: jwtConfig.ExpireTime,
		jKey:       []byte(secret),
	}
}

func (j *jwtManagement) Create(auth internals.AuthTokenPayload) (string, error) {
	expirationTime := time.Now().Add(5 * time.Minute)
	claims := &claims{
		Auth: internals.AuthTokenPayload{
			UserID: auth.UserID,
			Role:   auth.Role,
		},
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(j.jKey)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

func (j *jwtManagement) Verify(token string) (*internals.AuthTokenPayload,
	error) {
	var c claims
	_, err := jwt.ParseWithClaims(token, &c,
		func(token *jwt.Token) (interface{}, error) {
			return j.jKey, nil
		})
	if err != nil {
		return nil, err
	}
	return &c.Auth, nil
}
