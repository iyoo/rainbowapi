package auth

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/rs/zerolog/log"
	"gitlab.com/rainbowproject/rainbow-api/internal/messages"
	"gitlab.com/rainbowproject/rainbow-api/internal/models/rest"
	"io/ioutil"
	"net/http"
	"strings"
)

func MuxMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authorizationHeader := r.Header.Get("authorization")
		if authorizationHeader != "" {
			bearerToken := strings.Split(authorizationHeader, " ")
			if len(bearerToken) == 2 {
				token := bearerToken[1]
				tokenPayload, err := NewTokenHandler().Verify(token)
				if err != nil {
					e := rest.NewForbiddenError(messages.Forbidden)
					e.WriteResponse(w)
				} else {
					/* JWTToken ok - Check Policy Access */
					authReqPayload := rest.OpaInputReq{
						Input: rest.OpaReq{
							UserID: tokenPayload.UserID,
							Method: r.Method,
							Path:   r.URL.Path,
							Role:   tokenPayload.Role,
						},
					}
					if enforcePolicy(&authReqPayload) {
						ctx := r.Context()
						ctx = context.WithValue(ctx, "email",
							tokenPayload.UserID)
						next.ServeHTTP(w, r.WithContext(ctx))
					} else {
						e := rest.NewForbiddenError(messages.Forbidden)
						e.WriteResponse(w)
					}
				}
			} else {
				e := rest.NewForbiddenError(messages.InvalidToken)
				e.WriteResponse(w)
			}
		} else {
			e := rest.NewForbiddenError(messages.MissingToken)
			e.WriteResponse(w)
		}
	})
}

func enforcePolicy(r *rest.OpaInputReq) bool {
	jR, errm := json.Marshal(r)
	if errm != nil {
		panic(errm)
	}

	const opaURL = "http://localhost:8181/v1/data/rainbow/authz"
	resp, rErr := http.Post(opaURL, "application/json", bytes.NewBuffer(jR))
	if rErr != nil {
		log.Error().Msg(rErr.Error())
		return false
	}

	body, pErr := ioutil.ReadAll(resp.Body)
	if pErr != nil {
		log.Error().Msg(pErr.Error())
		return false
	}

	var opaResult rest.OpaResult
	uErr := json.Unmarshal(body, &opaResult)
	if uErr != nil {
		log.Error().Msg(uErr.Error())
		return false
	}

	log.Info().Msgf("Policy Enforce Result: %v", opaResult)
	return opaResult.Result.Allow
}
