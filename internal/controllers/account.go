package controllers

import (
	"database/sql"
	"gitlab.com/rainbowproject/rainbow-api/internal/models/rest"
	"net/http"

	"github.com/rs/zerolog/log"
	"gitlab.com/rainbowproject/rainbow-api/internal/messages"
	"gitlab.com/rainbowproject/rainbow-api/internal/services"
	"gitlab.com/rainbowproject/rainbow-api/internal/validator"
)

type AccountController interface {
	SignIn(w http.ResponseWriter, r *http.Request)
	Login(w http.ResponseWriter, r *http.Request)
	Suspend(w http.ResponseWriter, r *http.Request)
	Unsuspend(w http.ResponseWriter, r *http.Request)
	Delete(w http.ResponseWriter, r *http.Request)
	Restore(w http.ResponseWriter, r *http.Request)
	NewEmailCode(w http.ResponseWriter, r *http.Request)
	ValidateEmail(w http.ResponseWriter, r *http.Request)
	NewResetPassCode(w http.ResponseWriter, r *http.Request)
	ResetPass(w http.ResponseWriter, r *http.Request)
	UpdatePass(w http.ResponseWriter, r *http.Request)
}

type account struct {
	s services.AccountServiceHandler
}

func NewAccountController(db *sql.DB,
	v validator.ValidateJSON) *account {
	return &account{
		s: services.NewAccountServiceHandler(db, v),
	}
}

// swagger:operation POST /v1/account/signin account-api SignIn
//
// Create a new account
//
// ---
//
//     consumes: [application/json]
//     produces: [application/json]
//
//     parameters:
//     - name: body
//       in: body
//       description: Data to register a new account into the system
//       required: true
//       schema:
//         $ref: "#/definitions/AccountRegistrationReq"
//
//     responses:
//       200:
//         description: Success Message
//         schema:
//             $ref: "#/definitions/Suc"
//
func (u *account) SignIn(w http.ResponseWriter, r *http.Request) {
	log.Trace().Msg("CONTROLLER - Create a new account request")
	var input rest.AccountRegistrationReq
	eV := decode(r.Body, &input)
	if eV != nil {
		eV.WriteResponse(w)
		return
	}
	s, err := u.s.Create(input)
	if err != nil {
		err.WriteResponse(w)
		return
	}
	s.WriteResponse(w)
}

// swagger:operation PUT /v1/account/login account-api Login
//
// Login into the system
//
// ---
//
//     consumes: [application/json]
//     produces: [application/json]
//
//     parameters:
//     - name: body
//       in: body
//       description: Login Request
//       required: true
//       schema:
//         $ref: "#/definitions/LoginReq"
//
//     responses:
//       200:
//         description: Success Message
//         schema:
//             $ref: "#/definitions/Suc"
//
func (u *account) Login(w http.ResponseWriter, r *http.Request) {
	log.Trace().Msg("CONTROLLER - Account login request")
	var input rest.LoginReq
	eV := decode(r.Body, &input)
	if eV != nil {
		eV.WriteResponse(w)
		return
	}
	s, err := u.s.Login(input)
	if err != nil {
		err.WriteResponse(w)
		return
	}
	s.WriteResponse(w)
}

// swagger:operation PUT /v1/account/suspend account-api Suspend
//
// Suspend account
//
// ---
//
//     consumes: [application/json]
//     produces: [application/json]
//
//     parameters:
//     - name: body
//       in: body
//       description: Data to suspend an account
//       required: true
//       schema:
//         $ref: "#/definitions/AccountChangeStatusReq"
//
//     responses:
//       200:
//         description: Success Message
//         schema:
//             $ref: "#/definitions/Suc"
//
func (u *account) Suspend(w http.ResponseWriter, r *http.Request) {
	log.Trace().Msg("CONTROLLER - Suspend account request")
	var input rest.AccountChangeStatusReq
	eV := decode(r.Body, &input)
	if eV != nil {
		eV.WriteResponse(w)
		return
	}
	s, err := u.s.Suspend(input)
	if err != nil {
		err.WriteResponse(w)
		return
	}
	s.WriteResponse(w)
}

// swagger:operation PUT /v1/account/unsuspend account-api Unsuspend
//
// Unsuspend account
//
// ---
//
//     consumes: [application/json]
//     produces: [application/json]
//
//     parameters:
//     - name: body
//       in: body
//       description: Data to unsuspend an account
//       required: true
//       schema:
//         $ref: "#/definitions/AccountChangeStatusReq"
//
//     responses:
//       200:
//         description: Success Message
//         schema:
//             $ref: "#/definitions/Suc"
//
func (u *account) Unsuspend(w http.ResponseWriter, r *http.Request) {
	log.Trace().Msg("CONTROLLER - Unsuspend account request")
	var input rest.AccountChangeStatusReq
	eV := decode(r.Body, &input)
	if eV != nil {
		eV.WriteResponse(w)
		return
	}
	s, err := u.s.Unsuspend(input)
	if err != nil {
		err.WriteResponse(w)
		return
	}
	s.WriteResponse(w)
}

// swagger:operation PUT /v1/account/delete account-api Delete
//
// Mark account to be deleted
//
// ---
//
//     consumes: [application/json]
//     produces: [application/json]
//
//     parameters:
//     - name: body
//       in: body
//       description: Data to delete an account
//       required: true
//       schema:
//         $ref: "#/definitions/AccountChangeStatusReq"
//
//     responses:
//       200:
//         description: Success Message
//         schema:
//             $ref: "#/definitions/Suc"
//
func (u *account) Delete(w http.ResponseWriter, r *http.Request) {
	log.Trace().Msg("CONTROLLER - Mark account to be deleted")
	var input rest.AccountChangeStatusReq
	eV := decode(r.Body, &input)
	if eV != nil {
		eV.WriteResponse(w)
		return
	}
	s, err := u.s.Delete(input)
	if err != nil {
		err.WriteResponse(w)
		return
	}
	s.WriteResponse(w)
}

// swagger:operation PUT /v1/account/restore account-api Restore
//
// Try to restore account if is not already deleted
//
// ---
//
//     consumes: [application/json]
//     produces: [application/json]
//
//     parameters:
//     - name: body
//       in: body
//       description: Data to try to restore an account
//       required: true
//       schema:
//         $ref: "#/definitions/AccountChangeStatusReq"
//
//     responses:
//       200:
//         description: Success Message
//         schema:
//             $ref: "#/definitions/Suc"
//
func (u *account) Restore(w http.ResponseWriter, r *http.Request) {
	log.Trace().Msg("CONTROLLER - Try to restore marked to delete account")
	var input rest.AccountChangeStatusReq
	eV := decode(r.Body, &input)
	if eV != nil {
		eV.WriteResponse(w)
		return
	}
	s, err := u.s.Restore(input)
	if err != nil {
		err.WriteResponse(w)
		return
	}
	s.WriteResponse(w)
}

// swagger:operation POST /v1/email/newcode account-email-api NewEmailCode
//
// Generate a new confirmation code to send by email
//
// ---
//
//     consumes: [application/json]
//     produces: [application/json]
//
//     parameters:
//     - name: body
//       in: body
//       description: accout email to generate new code
//       required: true
//       schema:
//         $ref: "#/definitions/CodeReq"
//
//     responses:
//       200:
//         description: Success Message
//         schema:
//             $ref: "#/definitions/Suc"
//
func (u *account) NewEmailCode(w http.ResponseWriter,
	r *http.Request) {
	log.Trace().Msg("CONTROLLER - Email verifier UUID request")
	var input rest.CodeReq
	eV := decode(r.Body, &input)
	if eV != nil {
		eV.WriteResponse(w)
		return
	}
	s, err := u.s.NewCodeEmail(input)
	if err != nil {
		err.WriteResponse(w)
		return
	}
	s.WriteResponse(w)
}

// swagger:operation PUT /v1/email/validate account-email-api ValidateEmail
//
// Validate an account email address
//
// ---
//
//     consumes: [application/json]
//     produces: [application/json]
//
//     parameters:
//     - name: body
//       in: body
//       description: The account UUID got by email
//       required: true
//       schema:
//         $ref: "#/definitions/EmailValidationReq"
//
//     responses:
//       200:
//         description: Success Message
//         schema:
//             $ref: "#/definitions/Suc"
//
func (u *account) ValidateEmail(w http.ResponseWriter,
	r *http.Request) {
	log.Trace().Msg("CONTROLLER - Verify email by UUID request")
	var input rest.EmailValidationReq
	eV := decode(r.Body, &input)
	if eV != nil {
		eV.WriteResponse(w)
		return
	}
	s, err := u.s.ValidateEmail(input)
	if err != nil {
		err.WriteResponse(w)
		return
	}
	s.WriteResponse(w)
}

// swagger:operation POST /v1/pass/newcode account-pass-reset-api NewResetPassCode
//
// Request a password reset
//
// ---
//
//     consumes: [application/json]
//     produces: [application/json]
//
//     parameters:
//     - name: body
//       in: body
//       description: UUID got by email
//       required: true
//       schema:
//         $ref: "#/definitions/CodeReq"
//
//     responses:
//       200:
//         description: Success Message
//         schema:
//             $ref: "#/definitions/Suc"
//
func (u *account) NewResetPassCode(w http.ResponseWriter,
	r *http.Request) {
	log.Trace().Msg("CONTROLLER - Password reset request")
	var input rest.CodeReq
	eV := decode(r.Body, &input)
	if eV != nil {
		eV.WriteResponse(w)
		return
	}
	s, err := u.s.NewCodeResetPass(input)
	if err != nil {
		err.WriteResponse(w)
		return
	}
	s.WriteResponse(w)
}

// swagger:operation PUT /v1/pass/reset account-pass-reset-api ResetPass
//
// Reset account password
//
// ---
//
//     consumes: [application/json]
//     produces: [application/json]
//
//     parameters:
//     - name: body
//       in: body
//       description: Data to update account password
//       required: true
//       schema:
//         $ref: "#/definitions/ResetPassReq"
//
//     responses:
//       200:
//         description: Success Message
//         schema:
//           $ref: "#/definitions/Suc"
//
func (u *account) ResetPass(w http.ResponseWriter,
	r *http.Request) {
	log.Trace().Msg("CONTROLLER - Reset account password request")
	var input rest.ResetPassReq
	eV := decode(r.Body, &input)
	if eV != nil {
		eV.WriteResponse(w)
		return
	}
	s, err := u.s.ResetPassword(input)
	if err != nil {
		err.WriteResponse(w)
		return
	}
	s.WriteResponse(w)
}

// swagger:operation PUT /v1/account/pass account-api UpdatePass
//
// Update account password
//
// ---
//
//     consumes: [application/json]
//     produces: [application/json]
//
//     security:
//       - Bearer: []
//
//     parameters:
//     - name: body
//       in: body
//       description: Data to update account password
//       required: true
//       schema:
//         $ref: "#/definitions/UpdatePassReq"
//
//     responses:
//       200:
//         description: Success Message
//         schema:
//           $ref: "#/definitions/Suc"
//
func (u *account) UpdatePass(w http.ResponseWriter,
	r *http.Request) {
	log.Trace().Msg("CONTROLLER - Update account password request")
	email, ok := r.Context().Value("email").(string)
	if !ok {
		e := rest.NewInternalServerError(messages.BrokenToken)
		e.WriteResponse(w)
		return
	}
	var input rest.UpdatePassReq
	eV := decode(r.Body, &input)
	if eV != nil {
		eV.WriteResponse(w)
		return
	}
	s, err := u.s.UpdatePassword(email, input)
	if err != nil {
		err.WriteResponse(w)
		return
	}
	s.WriteResponse(w)
}
