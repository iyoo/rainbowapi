package controllers

import (
	"encoding/json"
	"gitlab.com/rainbowproject/rainbow-api/internal/models/rest"
	"io"

	"github.com/rs/zerolog/log"
	"gitlab.com/rainbowproject/rainbow-api/internal/messages"
)

func decode(r io.ReadCloser, i interface{}) rest.Err {
	if r == nil {
		msg := messages.ControllerDecodeNilBody
		log.Error().Msg(msg)
		return rest.NewBadRequestError(msg)
	}
	bodyDecoder := json.NewDecoder(r)
	bodyDecoderErr := bodyDecoder.Decode(i)
	if bodyDecoderErr != nil {
		log.Error().Msg(bodyDecoderErr.Error())
		return rest.NewInternalServerError(
			messages.ControolerDecodeFail)
	}
	defer r.Close()
	return nil
}
