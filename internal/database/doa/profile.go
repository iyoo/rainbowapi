package doa

import (
	"database/sql"
	"gitlab.com/rainbowproject/rainbow-api/internal/models/internals"
	"gitlab.com/rainbowproject/rainbow-api/internal/models/rest"

	"github.com/rs/zerolog/log"
	"gitlab.com/rainbowproject/rainbow-api/internal/messages"
)

type DAOProfile interface {
	FetchByAID(int) (*internals.Profile, rest.Err)
	UpdateAvatar(int, string) rest.Err
	UpdateInfo(*internals.Profile) rest.Err
	Delete(int) rest.Err
}

const (
	queryFetchProfileByAID   = "SELECT account_id, avatar, first_name, last_name, mobile, gender FROM profiles WHERE account_id=?"
	queryDeleteProfile       = "DELETE FROM profiles where account_id=?"
	queryUpdateProfileAvatar = "INSERT INTO profiles(account_id, avatar, first_name, last_name, mobile, gender) VALUES(?, ?, '', '', '', '') ON DUPLICATE KEY UPDATE avatar=?"
	queryUpdateProfileInfo   = "INSERT INTO profiles(account_id, avatar, first_name, last_name, mobile, gender) VALUES(?, '', ?, ?, ?, ?) ON DUPLICATE KEY UPDATE first_name=?, last_name=?, mobile=?, gender=?"
)

type mysqlProfile struct {
	db *sql.DB
}

func NewDOAProfileHandler(db *sql.DB) *mysqlProfile {
	profileHandler := mysqlProfile{
		db: db,
	}
	return &profileHandler
}

func (p *mysqlProfile) FetchByAID(aID int) (*internals.Profile,
	rest.Err) {
	log.Trace().Msg("DB - Fetch Account Profile")
	stmt, pErr := p.db.Prepare(queryFetchProfileByAID)
	if pErr != nil {
		e := rest.NewInternalServerError(messages.SqlPrepareFail)
		log.Error().Msg(pErr.Error())
		return nil, e
	}
	defer stmt.Close()
	var pP internals.Profile

	_ = stmt.QueryRow(aID).Scan(&pP.AccountID, &pP.Avatar, &pP.FirstName,
		&pP.LastName, &pP.Mobile, &pP.Gender)

	return &pP, nil
}

func (p *mysqlProfile) UpdateAvatar(aID int, avatarSrc string) rest.Err {
	log.Trace().Msg("DB - Update Profile Avatar")
	stmt, pErr := p.db.Prepare(queryUpdateProfileAvatar)
	if pErr != nil {
		e := rest.NewInternalServerError(messages.SqlPrepareFail)
		log.Error().Msg(pErr.Error())
		return e
	}
	defer stmt.Close()
	_, eErr := stmt.Exec(aID, avatarSrc, avatarSrc)
	if eErr != nil {
		e := rest.NewInternalServerError(messages.SqlFailExecute)
		log.Error().Msg(eErr.Error())
		return e
	}
	return nil
}

func (p *mysqlProfile) UpdateInfo(pP *internals.Profile) rest.Err {
	log.Trace().Msg("DB - Update Profile Info")
	stmt, pErr := p.db.Prepare(queryUpdateProfileInfo)
	if pErr != nil {
		e := rest.NewInternalServerError(messages.SqlPrepareFail)
		log.Error().Msg(pErr.Error())
		return e
	}
	defer stmt.Close()

	_, eErr := stmt.Exec(pP.AccountID, pP.FirstName, pP.LastName, pP.Mobile,
		pP.Gender, pP.FirstName, pP.LastName, pP.Mobile, pP.Gender)
	if eErr != nil {
		e := rest.NewInternalServerError(messages.SqlFailExecute)
		log.Error().Msg(eErr.Error())
		return e
	}
	return nil
}

func (p *mysqlProfile) Delete(aID int) rest.Err {
	log.Trace().Msg("DB - Delete Profile")
	stmt, pErr := p.db.Prepare(queryDeleteProfile)
	if pErr != nil {
		e := rest.NewInternalServerError(messages.SqlPrepareFail)
		log.Error().Msg(pErr.Error())
		return e
	}
	_, eErr := stmt.Exec(aID)
	if eErr != nil {
		e := rest.NewInternalServerError(messages.SqlFailExecute)
		log.Error().Msg(eErr.Error())
		return e
	}
	return nil
}
