package internals

type Profile struct {
	ID        int
	AccountID int
	Avatar    string
	FirstName string
	LastName  string
	Mobile    string
	Gender    string
}

type Privacy struct {
	ID            int
	AccountID     int
	PublicProfile bool
}

type Gender struct {
	ID   int
	Type string
}
