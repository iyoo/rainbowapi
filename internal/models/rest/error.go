package rest

import (
	"encoding/json"
	"net/http"

	"github.com/rs/zerolog/log"
)

type Err interface {
	WriteResponse(w http.ResponseWriter)
}

// Response Error Message
// swagger:model Err
type jsonErrorMsg struct {
	// Error Message
	// required: true
	// example: Invalid password
	Message string `json:"message"`
	// Error Message Code
	// required: true
	// example: 404
	Status int `json:"status"`
	// Error Message summary
	// required: true
	// example: "not_found"
	Error string `json:"error"`
}

func NewBadRequestError(msg string) Err {
	return newError(msg, http.StatusBadRequest)
}

func NewNotFoundError(msg string) Err {
	return newError(msg, http.StatusNotFound)
}

func NewInternalServerError(msg string) Err {
	return newError(msg, http.StatusInternalServerError)
}

func NewForbiddenError(msg string) Err {
	return newError(msg, http.StatusForbidden)
}

func newError(msg string, code int) Err {
	return &jsonErrorMsg{
		Message: msg,
		Status:  code,
		Error:   http.StatusText(code),
	}
}

func (e jsonErrorMsg) WriteResponse(w http.ResponseWriter) {
	jSon, err := json.Marshal(e)
	if err != nil {
		log.Error().Msg(err.Error())
		return
	}

	log.Debug().Msgf("-> ERR :: %d :: %s :: %s", e.Status, e.Error, e.Message)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(e.Status)
	_, err = w.Write(jSon)
	if err != nil {
		log.Error().Msg(err.Error())
	}
}
