package rest

import (
	"encoding/json"
	"net/http"

	"github.com/rs/zerolog/log"
)

type Suc interface {
	WriteResponse(w http.ResponseWriter)
}

// Response Success Message
// swagger:model Suc
type respSuc struct {
	// Success Message
	// required: true
	// example: User created
	Message string `json:"message"`
	// Success Code
	// required: true
	// example: 200
	Status int `json:"status"`
	// Success Payload
	// required: true
	// example: {}
	Payload interface{} `json:"payload,omitempty"`
}

/* ------------------------------------------------------------------------- */

func NewSuccessCreated(msg string, p interface{}) Suc {
	return newSuccess(msg, http.StatusCreated, p)
}

func NewBasicSuccess(msg string, p interface{}) Suc {
	return newSuccess(msg, http.StatusOK, p)
}

func newSuccess(msg string, status int, p interface{}) Suc {
	return &respSuc{
		Message: msg,
		Status:  status,
		Payload: p,
	}
}

func (s *respSuc) WriteResponse(w http.ResponseWriter) {
	jSon, err := json.Marshal(s)
	if err != nil {
		log.Error().Msg(err.Error())
		return
	}

	log.Debug().Msgf("-> SUC :: %d :: %s", s.Status, s.Message)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(s.Status)
	_, err = w.Write(jSon)
	if err != nil {
		log.Error().Msg(err.Error())
	}
}
