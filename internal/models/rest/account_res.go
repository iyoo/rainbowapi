package rest

// Bearer Token Payload
// swagger:model TokenResp
type TokenResp struct {
	// Bearer Token
	// unique: true
	// required: true
	// example: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiRnJlZWRvbSBvZiBleHByZXNzaW9uIiwibWVzc2FnZSI6ImxpdmUgbGlmZSBhcm91bmQgZXhwZXJpZW5jZSByYXRoZXIgdGhhbiBzdHVmZnwiLCJpYXQiOjE1MTYyMzkwMjJ9.2wZG5ykBQuqAg99OnHXJmyzWCBDkrKT2heODjuHxoGw
	BearerToken string `json:"bearer_token"`
}
