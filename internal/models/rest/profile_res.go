package rest

// Profile Info Response
// swagger:model ProfileInfoResp
type ProfileInfoResp struct {
	// Avatar
	// Min Length: 5
	// required: true
	// example: profile.jpg
	Avatar string
	// FistName
	// Min Length: 10
	// required: true
	// example: "Dummy"
	FirstName string `json:"first_name" validate:"required,gte=10"`
	// LastName
	// Min Length: 10
	// required: true
	// example: "DummyFamily"
	LastName string `json:"last_name" validate:"required,gte=10"`
	// Mobile
	// Min Length: 10
	// required: true
	// example: "48123456789"
	Mobile string `json:"mobile" validate:"required,mobile"`
	// Gender
	// Min Length: 10
	// required: true
	// example: "non-binary"
	Gender string `json:"gender" validate:"required,gte=5"`
}
