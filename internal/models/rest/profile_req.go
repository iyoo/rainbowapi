package rest

// Profile Info Create/Update Request
// swagger:model ProfileInfoReq
type ProfileInfoReq struct {
	// FistName
	// Max Lenght: 255
	// Min Length: 10
	// required: true
	// example: "Dummy"
	FirstName string `json:"first_name" validate:"required,gte=3"`
	// LastName
	// Max Lenght: 255
	// Min Length: 10
	// required: true
	// example: "DummyFamily"
	LastName string `json:"last_name" validate:"required,gte=3"`
	// Mobile
	// Max Lenght: 255
	// Min Length: 10
	// required: true
	// example: "48123456789"
	Mobile string `json:"mobile" validate:"required,gte=8"`
	// Gender
	// Max Lenght: 255
	// Min Length: 10
	// required: true
	// example: "non-binary"
	Gender string `json:"gender" validate:"required,gte=3"`
}

// Profile Avatar Update Request
// swagger:model ProfileAvatarReq
type ProfileAvatarReq struct {
	// Avatar
	// Min Length: 5
	// required: true
	// example: profile.jpg
	Avatar string
}
