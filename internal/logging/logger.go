package logging

import (
	"fmt"
	"gitlab.com/rainbowproject/rainbow-api/internal/config"
	"os"
	"strings"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

func InitLogger() {
	var logLevel zerolog.Level
	logLevelEnv := strings.ToLower(config.GetDebugLevelEnvVars())

	if logLevelEnv == "debug" {
		logLevel = zerolog.DebugLevel
	} else if logLevelEnv == "trace" {
		logLevel = zerolog.TraceLevel
	} else {
		logLevelEnv = "info"
		logLevel = zerolog.InfoLevel
	}

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zerolog.SetGlobalLevel(logLevel)
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	log.Logger = log.With().Caller().Logger()
	logLevelMsg := fmt.Sprintf("set log level to %s", logLevelEnv)
	log.Info().Msg(logLevelMsg)
	log.Trace().Msg("Logger Initialized")
}
