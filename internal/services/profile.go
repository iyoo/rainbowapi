package services

import (
	"database/sql"
	"fmt"
	"gitlab.com/rainbowproject/rainbow-api/internal/database/doa"
	"gitlab.com/rainbowproject/rainbow-api/internal/models/rest"
	"io/ioutil"
	"mime/multipart"
	"os"

	"github.com/rs/zerolog/log"

	"gitlab.com/rainbowproject/rainbow-api/internal/config"
	"gitlab.com/rainbowproject/rainbow-api/internal/messages"
	"gitlab.com/rainbowproject/rainbow-api/internal/models/internals"
	"gitlab.com/rainbowproject/rainbow-api/internal/validator"
)

type ProfileServiceHandler interface {
	FetchByAID(int) (rest.Suc, rest.Err)
	UpdateAvatar(int, multipart.File) (rest.Suc, rest.Err)
	UpdateProfile(int, *rest.ProfileInfoReq) (rest.Suc, rest.Err)
	Delete(int) (rest.Suc, rest.Err)
}

type profile struct {
	dbService doa.DAOProfile
	v         validator.ValidateJSON
	avatarCfg *config.AvatarConfig
}

func NewProfileServiceHandler(db *sql.DB,
	v validator.ValidateJSON) *profile {
	p := profile{
		dbService: doa.NewDOAProfileHandler(db),
		v:         v,
		avatarCfg: config.GetAvatarEnvVar(),
	}
	return &p
}

func (p *profile) FetchByAID(aID int) (
	rest.Suc, rest.Err) {
	log.Trace().Msg("SERVICE - Get profile by ID")
	privateProfile, fErr := p.dbService.FetchByAID(aID)
	if fErr != nil {
		return nil, fErr
	}
	profile := rest.ProfileInfoResp{
		Avatar:    privateProfile.Avatar,
		FirstName: privateProfile.FirstName,
		LastName:  privateProfile.LastName,
		Mobile:    privateProfile.Mobile,
		Gender:    privateProfile.Gender,
	}
	s := rest.NewBasicSuccess(messages.ProfileSucceed, profile)
	return s, nil
}

func (p *profile) UpdateAvatar(aID int, file multipart.File) (
	rest.Suc, rest.Err) {
	log.Trace().Msg("SERVICE - Update profile avatar")
	dir := p.getProfileAvatarFolder(aID)
	_ = os.MkdirAll(dir, os.ModePerm)
	avatarFile, fErr := ioutil.TempFile(dir, "avatar-*.png")
	if fErr != nil {
		e := rest.NewInternalServerError(
			messages.ProfileAvatarTempFail)
		log.Error().Msg(fErr.Error())
		return nil, e
	}
	fileBytes, rErr := ioutil.ReadAll(file)
	if rErr != nil {
		e := rest.NewInternalServerError(
			messages.ProfileAvatarReadFail)
		log.Error().Msg(rErr.Error())
		return nil, e
	}
	_, wErr := avatarFile.Write(fileBytes)
	if wErr != nil {
		e := rest.NewInternalServerError(
			messages.ProfileAvatarWriteFail)
		log.Error().Msg(wErr.Error())
		return nil, e
	}
	uErr := p.dbService.UpdateAvatar(aID, avatarFile.Name())
	if uErr != nil {
		return nil, uErr
	}
	s := rest.NewBasicSuccess(messages.ProfileAvatarUpdated, nil)
	return s, nil
}

func (p *profile) UpdateProfile(aID int,
	r *rest.ProfileInfoReq) (
	rest.Suc, rest.Err) {
	log.Trace().Msg("SERVICE - Update profile info")
	vErr := p.v.Validate(r)
	if vErr != nil {
		log.Error().Msg(vErr.Error())
		e := rest.NewBadRequestError(vErr.Error())
		return nil, e
	}
	pP := internals.Profile{
		AccountID: aID,
		FirstName: r.FirstName,
		LastName:  r.LastName,
		Mobile:    r.Mobile,
		Gender:    r.Gender,
	}
	uErr := p.dbService.UpdateInfo(&pP)
	if uErr != nil {
		return nil, uErr
	}
	s := rest.NewBasicSuccess(messages.ProfileInfoUpdated, nil)
	return s, nil
}

func (p *profile) Delete(aID int) (
	rest.Suc, rest.Err) {
	log.Trace().Msg("SERVICE - Delete profile by ID")
	dErr := p.dbService.Delete(aID)
	rErr := os.RemoveAll(p.getProfileBaseFolder(aID))
	if rErr != nil {
		log.Error().Msg(rErr.Error())
	}
	if dErr != nil {
		return nil, dErr
	}
	s := rest.NewBasicSuccess(messages.ProfileDeleted, nil)
	return s, nil
}

func (p *profile) getProfileBaseFolder(aID int) string {
	return fmt.Sprintf("static/users/%d/profile", aID)
}

func (p *profile) getProfileAvatarFolder(aID int) string {
	return fmt.Sprintf("%s/%s", p.getProfileBaseFolder(aID), "images/avatar/")
}
