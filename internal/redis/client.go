package redis

import (
	"time"

	"github.com/go-redis/redis"
	"gitlab.com/rainbowproject/rainbow-api/internal/config"
)

type ClientHandler interface {
	Get(string) (string, error)
	Set(string, interface{}, time.Duration) (string, error)
	Del(string) (int64, error)
}

type redisClient struct {
	c *redis.Client
}

func NewRedisEmailHandler() *redisClient {
	cfg := config.GetRedisEmailEvnVars()
	return &redisClient{
		c: redis.NewClient(&redis.Options{
			Addr: cfg.Host +
				":" + cfg.Port,
			Password: cfg.Pass,
			DB:       0, // use default DB
		}),
	}
}

func NewRedisPassHandler() ClientHandler {
	cfg := config.GetRedisPassEvnVars()
	return &redisClient{
		c: redis.NewClient(&redis.Options{
			Addr: cfg.Host +
				":" + cfg.Port,
			Password: cfg.Pass,
			DB:       0, // use default DB
		}),
	}
}

func (r *redisClient) Get(k string) (string, error) {
	return r.c.Get(k).Result()
}
func (r *redisClient) Set(k string, v interface{},
	t time.Duration) (string, error) {
	return r.c.Set(k, v, t).Result()
}
func (r *redisClient) Del(k string) (int64, error) {
	return r.c.Del(k).Result()
}
