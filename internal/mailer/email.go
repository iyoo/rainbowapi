package mailer

import (
	"bytes"
	"fmt"
	"gitlab.com/rainbowproject/rainbow-api/internal/redis"
	"html/template"
	"net/smtp"
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/rainbowproject/rainbow-api/internal/config"
)

type EmailHandler interface {
	SendUUIDEmailValidation(redis.ClientHandler, string)
	SendUUIDPassReset(redis.ClientHandler, string)
}

type smtpHandler struct {
	auth smtp.Auth
	host string
	from string
}

type emailTemplate struct {
	AppName string
	Code    string
}

func NewEmailHandler() *smtpHandler {
	sC := config.GetSMTPEnvVars()
	auth := smtp.PlainAuth("", sC.User, sC.Pass, sC.Host)
	host := fmt.Sprintf("%s:%s", sC.Host, sC.Port)
	return &smtpHandler{
		auth: auth,
		host: host,
		from: sC.From,
	}
}

func (e *smtpHandler) SendUUIDEmailValidation(r redis.ClientHandler,
	email string) {
	log.Trace().Msg("EMAIL - Send UUID code to account email - Email verifier")
	const newUserFile = "web/templates/email_verify.html"
	code := NewVerificationCodeHandler().NewCode()
	SendCodeByEmail(r, e, email, newUserFile,
		code, "Welcome!")
}

func (e *smtpHandler) SendUUIDPassReset(r redis.ClientHandler,
	email string) {
	log.Trace().Msg("EMAIL - Send UUID code to account email - Reset password")
	const passResetFile = "web/templates/password_reset.html"
	code := NewVerificationCodeHandler().NewCode()
	SendCodeByEmail(r, e, email, passResetFile,
		code, "Hello!")
}

func SendCodeByEmail(r redis.ClientHandler, e *smtpHandler, email string,
	fileName string, code string, subject string) {
	const (
		title    = "Generate code, Set Data on Redis and Send Email"
		redisErr = "Redis is not initialized"
	)

	log.Trace().Msg(title)
	if r == nil {
		log.Error().Msg(redisErr)
		return
	}

	_, rSetErr := r.Set(code, email, 24*time.Hour)
	if rSetErr != nil {
		log.Error().Msg(rSetErr.Error())
		return
	}

	var body bytes.Buffer
	headers := "MIME-version: 1.0;\nContent-Type: text/html"
	bodyData := fmt.Sprintf("Subject: %s\n%s\n\n", subject, headers)

	emailData := emailTemplate{
		AppName: config.GetAppName(),
		Code:    code,
	}
	to := []string{email}
	body.Write([]byte(bodyData))

	tpl, err := template.ParseFiles(fileName)
	if err != nil {
		log.Error().Msg(err.Error())
		return
	}

	tErr := tpl.Execute(&body, emailData)
	if tErr != nil {
		log.Error().Msg(tErr.Error())
		return
	}

	rSendErr := smtp.SendMail(e.host, e.auth, e.from, to, body.Bytes())
	if rSendErr != nil {
		log.Error().Msg(rSendErr.Error())
		return
	}
}
